.. Reusing Shape Keys documentation master file, created by
   sphinx-quickstart on Thu Feb 18 14:32:03 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Reusing Shape Keys's documentation!
==============================================

Копирование **Shape Keys** между различными сетками.

.. toctree::
   :maxdepth: 2
   
   gui



.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
